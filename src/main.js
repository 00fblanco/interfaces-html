import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import store  from './store'
import locale from 'element-ui/lib/locale/lang/es'

import "prismjs";
import "prismjs/themes/prism.css";
import VuePrismEditor from "vue-prism-editor";
import "vue-prism-editor/dist/VuePrismEditor.css"; // import the styles

// only import the icons you use to reduce bundle size
import 'vue-awesome/icons/flag'

// or import all icons if you don't care about bundle size
import 'vue-awesome/icons'

/* Register component with one of 2 methods */

import Icon from 'vue-awesome/components/Icon'



// globally (in your main .js file)
Vue.component('v-icon', Icon)

Vue.component("prism-editor", VuePrismEditor);

Vue.component('v-style', {
  render: function (createElement) {
    return createElement('style', this.$slots.default)
  }
});




Vue.use(ElementUI, { locale })

Vue.config.productionTip = false
window.Store = store; 
new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
