import Vue from 'vue';
import Vuex from 'vuex';
import { arch } from 'os';

Vue.use(Vuex);
const store = new Vuex.Store({
    state: {
        htmlbody: ``,
        elementos: [],
        elemento_id: 1,
        herramientasTabSelected: '',

        elementoSeleccionado: null,

        archivos: [],
        archivoSeleccionado: null
    },
    mutations: {
        SET_ELEMENTO(state, tipo){
            var archivoSeleccionado = state.archivos[state.archivoSeleccionado];
            if(archivoSeleccionado != null){
                var contenido = (tipo == 'input' || tipo == 'div') ? '' : 'Hola mundo';
                const el = {
                    id: archivoSeleccionado.elemento_id,
                    tipo,
                    contenido,
                    style: {
                        color: '#292d3e',
                        position: 'absolute',
                        left: 0,
                        top: 0,
                        width: 20,
                        height: 10,
                        background: '#fff',
                        margin_top: 0,
                        tamanio_letra: 20
                    },
                    tipo_input: '',
                    sl_opciones: [],
                    t_rows: 1,
                    t_cols: 1,
                    background_src: null
                }
                
            
                archivoSeleccionado.elemento_id += 1;
                archivoSeleccionado.elementos.push(el);
                state.archivos[state.archivoSeleccionado] = archivoSeleccionado;
            }            
        },
        ON_PROPIEDADES_TAB(state, el){
            state.elementoSeleccionado = el;
            //state.herramientasTabSelected = "1";
        },
        ON_COMPONENTES_TAB(state){
            state.herramientasTabSelected = "0";
        },
        ACTUALIZAR_ELEMENTO(state, newData){
            var archivoSeleccionado = state.archivos[state.archivoSeleccionado];
            if(archivoSeleccionado != null){

                archivoSeleccionado.elementos = archivoSeleccionado.elementos.map(el => {
                    if(el.id != state.elementoSeleccionado.id)
                        return {...el}
                    return {
                        ...el,
                        ...newData
                    }
                })
                state.archivos[state.archivoSeleccionado] = archivoSeleccionado;
            }  
        },
        ELIMINAR_ELEMENTO(state){
            var archivoSeleccionado = state.archivos[state.archivoSeleccionado];
            if(archivoSeleccionado != null){
                archivoSeleccionado.elementos = archivoSeleccionado.elementos = archivoSeleccionado.elementos.filter(el => {
                    return el.id != state.elementoSeleccionado.id
                });
                state.elementoSeleccionado = null
                state.herramientasTabSelected = "0";
            
            }
        },
        SET_POSITION_ELEMENTO(state, coords){
            state.elementoSeleccionado
        },
        ON_TAB(state, tab){
            state.herramientasTabSelected = `${tab}`
        },
        ADD_ARCHIVO(state, nombre){
            state.archivos.push({
                elemento_id: 1,
                nombre: `${nombre}.html`,
                htmlbody: ``,
                elementos: [],
                elemento_id: 1,
            });
        },
        SELECT_ARCHIVO(state, position){
            state.archivoSeleccionado = position;
            state.elementoSeleccionado = null;
            state.herramientasTabSelected = "0";
        }
    },
    actions: {
        agregarElemento(context, payload){
            context.commit('SET_ELEMENTO', payload)
        },
        onPropiedadesTab(context, payload){
            context.commit('ON_PROPIEDADES_TAB', payload);
        },
        onComponentesTab(context){
            context.commit('ON_COMPONENTES_TAB');
        },
        actualizarElemento(context, payload){
            context.commit('ACTUALIZAR_ELEMENTO', payload);
        },
        eliminarElemento(context){
            context.commit('ELIMINAR_ELEMENTO');
        },
        setPositionElemento(context, coords){
            context.commit('SET_POSITION_ELEMENTO', coords)
        },
        onTab(context, payload){
            context.commit('ON_TAB', payload);
        },
        addArchivo(context, nombre){
            context.commit('ADD_ARCHIVO', nombre);
        },
        selectArchivo(context, position){
            context.commit('SELECT_ARCHIVO', position)
        }
    },
    getters: {
        download_html: state => {
            var archivoSeleccionado = state.archivos[state.archivoSeleccionado];
            if(archivoSeleccionado != null){
                var html = `<!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <meta http-equiv="X-UA-Compatible" content="ie=edge">
                    <title>${archivoSeleccionado.nombre}</title>
                    <link rel="stylesheet" href='${archivoSeleccionado.nombre.replace('html', 'css')}'/>
                </head>
                <body>
                
                `;
                if(archivoSeleccionado.elementos.length == 0)
                    return "";
                html += archivoSeleccionado.elementos.map(el => {
                    var el_html = `<${el.tipo} id='el-design-${el.id}' `;
                    
                    switch(el.tipo){
                        case 'input':
                            if(el.tipo_input == 'checkbox' | el.tipo_input == 'radio'){
                                el_html += `display:inline;" type="${el.tipo_input}"/><label for="el-design-${el.id}" style="color: ${el.style.color};position: absolute; top: ${el.style.top+3}%; left: ${el.style.left+2}%;background-color: ${el.style.background}; margin: 0px 0px 0px 0px;">${el.contenido}</label>`;
                            }else{
                                el_html += ` " type="${el.tipo_input}" ></${el.tipo}>`;
                            }
                            //el_html += ` type="${el.tipo_input}"></${el.tipo}>`;
                            break;
                        case 'select':
                            var options = el.sl_opciones.map((val, index) => {
                                return `<option value="${val}">${val}</option>`;
                            });
                            el_html += `> ${options.join(" ")} </${el.tipo}>`;
                            break;
                        case 'table':
                            var rows = '';
                            for(var i = 0; i< el.t_rows; i++){
                                rows += "<tr>";

                                for(var j = 0; j< el.t_cols; j++){
                                    rows += "<td></td>"
                                }
                                rows += "</tr>";
                            }
                            el_html += `> ${rows} </${el.tipo}>`;
                            break;
                        case 'div':
                            el_html += `>  </${el.tipo}>`;
                            break;
                        default:
                            el_html += `> ${el.contenido} </${el.tipo}>`;
                            break;
                    }
                    return el_html; 
                }).join('\n');

                return html += `
                
                </body>
                </html>`;
                
            }
        },
        download_css: state => {
            var archivoSeleccionado = state.archivos[state.archivoSeleccionado];
            if(archivoSeleccionado != null){
                if(archivoSeleccionado.elementos.length == 0)
                    return "";
                return archivoSeleccionado.elementos.map(el => {
                    var el_css = `
                        #el-design-${el.id} {
                            font-size: ${el.style.tamanio_letra}px;
                            color: ${el.style.color};
                            position: ${el.style.position};
                            top: ${el.style.top}%; 
                            left: ${el.style.left}%; 
                            width: ${el.style.width}%; 
                            height: ${el.style.height}%;
                            background-color: ${el.style.background}; 
                            margin: 0px 0px 0px 0px;
                        
                        `;
                    switch(el.tipo){
                        case 'div':
                            if(el.background_src != null){
                                el_css += `
                                background-image: url(${el.background_src});
                                background-repeat: no-repeat;
                                background-size: contain;`;
                            }else{
                                el_css += `}`;
                            }

                            break;
                        default:
                            el_css += `}`;
                            break;
                    }
                    el_css = `${el_css}
                        tr{
                            border: 1px solid black;
                        }
                        td{
                            border: 1px solid black;
                        }
                    `
                    return el_css; 
                }).join('\n\n');
            }
        },
        htmlFile: state => {
            var archivoSeleccionado = state.archivos[state.archivoSeleccionado];
            if(archivoSeleccionado != null){
                if(archivoSeleccionado.elementos.length == 0)
                    return "";
                return archivoSeleccionado.elementos.map(el => {
                    var el_html = `<${el.tipo} id='el-design-${el.id}' style="font-size: ${el.style.tamanio_letra}px;color: ${el.style.color};position: ${el.style.position}; top: ${el.style.top}%; left: ${el.style.left}%; width: ${el.style.width}%; height: ${el.style.height}%;background-color: ${el.style.background}; margin: 0px 0px 0px 0px;`;
                    
                    switch(el.tipo){
                        case 'input':
                            if(el.tipo_input == 'checkbox' | el.tipo_input == 'radio'){
                                el_html += `display:inline;" type="${el.tipo_input}"/><label for="el-design-${el.id}" style="color: ${el.style.color};position: absolute; top: ${el.style.top+3}%; left: ${el.style.left+2}%;background-color: ${el.style.background}; margin: 0px 0px 0px 0px;">${el.contenido}</label>`;
                            }else{
                                el_html += ` " type="${el.tipo_input}" ></${el.tipo}>`;
                            }
                            
                            break;
                        case 'select':
                            var options = el.sl_opciones.map((val, index) => {
                                return `<option value="${val}">${val}</option>`;
                            });
                            el_html += `"> ${options.join(" ")} </${el.tipo}>`;
                            break;
                        case 'table':
                            var rows = '';
                            for(var i = 0; i< el.t_rows; i++){
                                rows += "<tr>";

                                for(var j = 0; j< el.t_cols; j++){
                                    rows += "<td></td>"
                                }
                                rows += "</tr>";
                            }
                            el_html += `"> ${rows} </${el.tipo}>`;
                            break;
                        case 'div':
                            if(el.background_src != null){
                                el_html += `;background-image: url(${el.background_src});background-repeat: no-repeat;background-size: contain;">  </${el.tipo}>`;
                            }else{
                                el_html += `">  </${el.tipo}>`;
                            }

                            break;
                        default:
                            el_html += ` "> ${el.contenido} </${el.tipo}>`;
                            break;
                    }
                    
                    return el_html; 
                }).join('\n');
            }
        },
        elementos: state => {
            var archivoSeleccionado = state.archivos[state.archivoSeleccionado];
            if(archivoSeleccionado != null){
                return archivoSeleccionado.elementos
            }
        },
        herramientasTabSelected: state => {
            return state.herramientasTabSelected
        },
        elementoSeleccionado: state => {
            return state.elementoSeleccionado
        },
        archivos: state => {
            return state.archivos;
        },
        archivoSeleccionado: state => {
            return state.archivos[state.archivoSeleccionado];
        },
        indexArchivoSeleccionado: state => {
            return state.archivoSeleccionado;
        }
    }
})

export default store;